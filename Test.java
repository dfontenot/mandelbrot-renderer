public class Test
{
    private static int MAX_COLOR_BIT = 255;
    
    public static void main(String[] args)
    {
	int max = 256 * 256 * 256;
	int r;
	int g;
	int b;
	for(int i = 256 * 256; i <= max; i++)
	{
	    r = (i / (int)Math.pow(MAX_COLOR_BIT + 1, 2)) % (MAX_COLOR_BIT + 1);
	    g = (i / (MAX_COLOR_BIT + 1)) % (MAX_COLOR_BIT + 1);
	    b = i % (MAX_COLOR_BIT + 1);
	    System.out.println(r + " " + g + " " + b);
	}
    }
}
