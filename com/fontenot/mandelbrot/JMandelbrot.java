package com.fontenot.mandelbrot;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Dimension;

import javax.swing.JComponent;

public class JMandelbrot extends JComponent {
    private BufferedImage fractal;
    private RenderingManager manageThread;

    // dirty specifies if a re-render is needed
    public volatile boolean dirty;

    public JMandelbrot()
    {
	dirty = true;		// need to render the image in the first place
	
	Dimension imageSize = MandelbrotSettings.imageSize;
        fractal = new BufferedImage(imageSize.width, imageSize.height, BufferedImage.TYPE_INT_RGB);
        setPreferredSize(imageSize);

	addMouseListener(new ZoomMouseListener(this));

	manageThread = new RenderingManager(this);
	manageThread.start();
    }

    public BufferedImage getFractal()
    {
	return fractal;
    }

    public boolean isImageRendering()
    {
	return !manageThread.isComplete();
    }

    public void restartThreads()
    {
	manageThread.restartThreads();
    }

    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        g.drawImage(fractal, 0, 0, getWidth(), getHeight(), this);
    }
}
