package com.fontenot.mandelbrot;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.MouseInfo;

public class ZoomMouseListener extends MouseAdapter
{
    private JMandelbrot parent;
    private int numButtons;

    public ZoomMouseListener(JMandelbrot p)
    {
	super();
	
	parent = p;
	numButtons = MouseInfo.getNumberOfButtons();
    }
    
    public void mouseClicked(MouseEvent evt)
    {
	// ignore all click events while image is rendering
	if(parent.isImageRendering())
	    return;

	if (evt.getClickCount() == 2 &&
	    evt.getButton() == MouseEvent.BUTTON1)
	{
	    MandelbrotSettings.zoom *= MandelbrotSettings.ZOOM_INCR;
	    MandelbrotSettings.lookAt = evt.getPoint();
	    parent.restartThreads();
	    parent.dirty = true;
	}

	// reset zoom level
	if((numButtons > 2 && evt.getButton() == MouseEvent.BUTTON3) ||
	   (numButtons == 2 && evt.getButton() == MouseEvent.BUTTON2))
	{
	    if(MandelbrotSettings.zoom == MandelbrotSettings.originalZoom)
		return;

	    MandelbrotSettings.zoom = MandelbrotSettings.originalZoom;
	    MandelbrotSettings.lookAt = MandelbrotSettings.center;
	    parent.restartThreads();
	    parent.dirty = true;
	}
    }
}
