package com.fontenot.mandelbrot;

import java.awt.Dimension;
import java.awt.Point;

// essentially a static class (no static top level classes in Java)
public class MandelbrotSettings
{
    private static final int DEFAULT_WIDTH = 500;
    private static final int DEFAULT_HEIGHT = 400;
    private static final int DEFAULT_ITERATIONS = 600;
    private static final int DEFAULT_THREAD_COUNT = 5;
    private static final double DEFAULT_ZOOM = 100.0;

    public static final double ZOOM_INCR = 1.2;
    
    public static int iterations = DEFAULT_ITERATIONS;
    public static int threadCount = DEFAULT_THREAD_COUNT;
    public static Dimension imageSize = new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    public static Point center = new Point(DEFAULT_WIDTH / 2, DEFAULT_HEIGHT / 2);	// center of the fractal
    public static Point lookAt = new Point(center);
    public static double zoom = DEFAULT_ZOOM;
    public static double originalZoom = DEFAULT_ZOOM;
}
