package com.fontenot.mandelbrot;

import java.math.BigDecimal;
import java.awt.image.BufferedImage;

// TODO: make into static class
public class MandelbrotRenderer {
    private static final int MAX_COLOR_VALUE = 16777216; // 256^3
    private static final int MAX_COLOR_BIT = 256;
    private static final double CIRCLE_DIAMETER = 4.0;

    private int[] fractal;
    
    private int rgbStep;
    private int width;		// store width to declutter code
    
    public MandelbrotRenderer(int[] fractal) {
	// TODO: guard against user choosing iterations greater than 255^3
	// or, don't map each iteration to a unique color
        rgbStep = MAX_COLOR_VALUE / MandelbrotSettings.iterations;
	width = MandelbrotSettings.imageSize.width;

	this.fractal = fractal;
        
        // zoom = BigDecimal.valueOf(DEFAULT_ZOOM);
        // zoom = DEFAULT_ZOOM;
    }

    public void renderLine(int l)
    {
	double yadj = (MandelbrotSettings.lookAt.y - MandelbrotSettings.center.y);
	double xadj = (MandelbrotSettings.lookAt.x - MandelbrotSettings.center.x);
	
        double ytemp = ((double)(MandelbrotSettings.center.y - l + yadj)) / MandelbrotSettings.zoom;
        
        //ytemp = BigDecimal.valueOf(k - l);
        //ytemp = ytemp.divide(zoom);

        for(int x = 0; x < width; x++)
	{
	    double xtemp = ((double)(MandelbrotSettings.center.x - x + xadj)) / MandelbrotSettings.zoom;
            
	    // xtemp = BigDecimal.valueOf(x - h);
	    // xtemp = xtemp.divide(zoom);

            fractal[(l * width) + x] = renderPoint(xtemp, ytemp);
	}
    }
    
    //renders the pixel value (int encoded) for a specific point
    // private int renderPoint(BigDecimal real, BigDecimal imag) {
    //     realTemp = BigDecimal.valueOf(0.0);
    //     imagTemp = BigDecimal.valueOf(0.0);

    //     for(int i = 0; i < MAX_COLOR_VALUE; i += rgbStep) {
    //         //does this value exceed the circle bounds?
            
    //         temp = realTemp.pow(2);
    //         temp = temp.add(imagTemp.pow(2));
    //         if(temp.compareTo(BigDecimal.valueOf(CIRCLE_DIAMETER)) == -1) {
    //             temp = realTemp.pow(2);
    //             temp = temp.subtract(imagTemp.pow(2));
    //             temp = temp.add(real);
                        
    //             imagTemp = imagTemp.multiply(BigDecimal.valueOf(2)).multiply(realTemp);
    //             imagTemp = imagTemp.add(imag);
                        
    //             realTemp = temp.divide(BigDecimal.valueOf(1));
    //         }
    //         else {
    //             int r = (i / MAX_COLOR_VALUE) % MAX_COLOR_BIT;
    //             int g = (i / (int)(Math.pow(MAX_COLOR_BIT, 2))) % MAX_COLOR_BIT;
    //             int b = i % MAX_COLOR_BIT;
    //             return ((r << 16) | (g << 8) | b);
    //         }
    //     }
	
    //     return 0;
    // }

    
    private int renderPoint(double real, double imag)
    {
        //tracks the actual value
        double real2 = 0.0;
        double imag2 = 0.0;
        double temp = 0.0;

	int r = 0;
	int g = 0;
	int b = 0;
	for(int i = 0; i < MAX_COLOR_VALUE; i += rgbStep)
	{
	    if(((real2 * real2) + (imag2 * imag2)) <= CIRCLE_DIAMETER)
	    {
		temp = (real2 * real2) - (imag2 * imag2) + real;
		imag2 = (2 * real2 * imag2) + imag;
		real2 = temp;
	    }
	    else
	    {
		r = (i / (int)Math.pow(MAX_COLOR_BIT, 2)) % MAX_COLOR_BIT;
                g = (i / MAX_COLOR_BIT) % MAX_COLOR_BIT;
                b = i % MAX_COLOR_BIT;
                return ((r << 16) | (g << 8) | b);
	    }
	}
        
        return 0;
    }
}
