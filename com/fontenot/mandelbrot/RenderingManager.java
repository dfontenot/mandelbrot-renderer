package com.fontenot.mandelbrot;

import java.awt.image.BufferedImage;
public class RenderingManager extends Thread
{
    private int[] fractalBuffer; // threads draw to this buffer
    private BufferedImage fractal;
    
    private RenderingThread[] rendThreads;
    private JMandelbrot parent;

    private boolean imageRendered;
    private Object completeLock = new Object();
    
    public RenderingManager(JMandelbrot parent)
    {
        super();

	this.parent = parent;
	imageRendered = false;

	fractal = parent.getFractal();
	fractalBuffer = new int[MandelbrotSettings.imageSize.width *
				MandelbrotSettings.imageSize.height];

	// spawn threads
	rendThreads = new RenderingThread[MandelbrotSettings.threadCount];
	for(int i = 0; i < MandelbrotSettings.threadCount; i++)
	{
	    rendThreads[i] = new RenderingThread(i, fractalBuffer);
	    rendThreads[i].start();
	}
    }

    public synchronized void restartThreads()
    {
	for(int i = 0; i < MandelbrotSettings.threadCount; i++)
	{
	    rendThreads[i].complete = false;
	}
    }
    
    public void run()
    {
	while(true)
	{
	    updateFractal();
	    yield();
	}
    }
    
    public void updateFractal()
    {
        if(parent.dirty)
        {
	    boolean subsectionsComplete = true;
	    for(int i = 0; i < MandelbrotSettings.threadCount; i++)
	    {
		subsectionsComplete = subsectionsComplete && rendThreads[i].complete;
	    }

	    if(subsectionsComplete)
	    {
		synchronized(completeLock)
		{
		    // write buffer to fractal
		    fractal.setRGB(0, 0, MandelbrotSettings.imageSize.width, MandelbrotSettings.imageSize.height,
				   fractalBuffer, 0, MandelbrotSettings.imageSize.width);
		    parent.repaint();
		    imageRendered = true;
		    parent.dirty = false;
		}
	    }
        }
    }

    public boolean isComplete()
    {
	synchronized(completeLock)
	{
	    return imageRendered;
	}
    }
}
