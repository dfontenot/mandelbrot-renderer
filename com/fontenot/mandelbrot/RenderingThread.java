package com.fontenot.mandelbrot;

//this thread can receive messages from swing about changing the zoom
public class RenderingThread extends Thread {
    private int[] buffer;
    private MandelbrotRenderer rend;
    private int index;		// represents the thread's index, which line it starts at

    public volatile boolean complete;

    public RenderingThread(int index, int[] fractalBuffer)
    {
        super();
	
	complete = false;
        rend = new MandelbrotRenderer(fractalBuffer);
        this.index = index;
    }
    
    public void run()
    {
	int height = MandelbrotSettings.imageSize.height;
	int threadCount = MandelbrotSettings.threadCount;
	
	while(true)
	{
	    if(!complete)
	    {
		for(int i = index; i < height; i += threadCount)
		{
		    rend.renderLine(i);
		}

		complete = true;
	    }

	    yield();
	}
    }
}
