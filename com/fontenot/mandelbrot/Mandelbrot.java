package com.fontenot.mandelbrot;

import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.Point;

public class Mandelbrot extends JFrame
{
    public Mandelbrot() {
        super("Mandelbrot by David Fontenot (c) 2010");
        
        add(new JMandelbrot());
        setVisible(true);
	setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }

    private static boolean stringMatchesArrIgnoreCase(String cmp, String... opts)
    {
	for(String opt : opts)
	{
	    if(cmp.equalsIgnoreCase(opt))
		return true;
	}

	return false;
    }

    /*
     * really basic argument reader ...no getopt in Java
     * doesn't guard against specifying an argument twice
     */
    private static void readArguments(String[] args) throws IllegalStateException
    {
	int argIdx = 0;		// first argument is all args
	String curArg;

	while(argIdx < args.length)
	{
	    curArg = args[argIdx++];

	    String peakArg = "";
	    try
	    {
		peakArg = args[argIdx++];
		
		if(stringMatchesArrIgnoreCase(curArg, "--iters", "--iterations", "-i"))
		    MandelbrotSettings.iterations = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "-t", "--threads"))
		    MandelbrotSettings.threadCount = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "-w", "--width"))
		    MandelbrotSettings.imageSize.width = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "-h", "--height"))
		    MandelbrotSettings.imageSize.height = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "--centerx"))
		    MandelbrotSettings.center.x = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "--centery"))
		    MandelbrotSettings.center.y = Integer.decode(peakArg);
		else if(stringMatchesArrIgnoreCase(curArg, "-z", "--zoom"))
		    MandelbrotSettings.zoom = MandelbrotSettings.originalZoom = Integer.decode(peakArg);
		else
		    argIdx--;
	    }
	    catch(ArrayIndexOutOfBoundsException aioobe)
	    {
		throw new IllegalStateException("Missing argument for command switch " + curArg);
	    }
	    catch(NumberFormatException nfe)
	    {
		throw new IllegalStateException("Argument " + peakArg + " at position " + argIdx + " is not a valid integer\n");
	    }
	}
    }

    public static void main(String[] args) {
	try
	{
	    readArguments(args);
	}
	catch(IllegalStateException ise)
	{
	    System.out.print(ise.getMessage());
	    return;
	}

	new Mandelbrot();
    }
}
