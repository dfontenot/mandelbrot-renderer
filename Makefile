CC=javac
JFLAGS=-g
JAR=jar
PREFIX=com/fontenot/mandelbrot/
SOURCES=$(wildcard $(PREFIX)*.java)

.PHONY: all clean run debug
all: jmandelbrot

run:
	java -jar jmandelbrot.jar

debug:
	jdb -sourcepath $(PREFIX) com.fontenot.mandelbrot.Mandelbrot

jmandelbrot: $(SOURCES)
	$(CC) $(JFLAGS) $^
	$(JAR) cmf manifest $@.jar $(PREFIX)*.class

clean: $(wildcard $(PREFIX)*.class) jmandelbrot.jar
	rm $^
